﻿namespace FunPay.Client.Core.Models.Sales
{
  public class Sale
  {
    public string Id { get; set; }
    public string Title { get; set; }
    public long BuyerId { get; set; }
    public Status Status { get; set; }
  }
}