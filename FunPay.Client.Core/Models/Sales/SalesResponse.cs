﻿using System.Collections.Generic;

namespace FunPay.Client.Core.Models.Sales
{
  public class SalesResponse : BaseFunPayResponse
  {
    public IEnumerable<Sale> Sales { get; set; }
  }

  public enum Status
  {
    Paid,
    Closed,
    Refund,
    Unknown
  }
}