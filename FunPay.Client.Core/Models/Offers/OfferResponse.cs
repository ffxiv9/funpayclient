﻿using FunPay.Client.Core.Interfaces.Offers;

namespace FunPay.Client.Core.Models.Offers
{
  public class OfferResponse : BaseFunPayResponse
  {
    public IOffer Offer { get; set; }
  }
}