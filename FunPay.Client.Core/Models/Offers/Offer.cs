﻿using FunPay.Client.Core.Enums;
using FunPay.Client.Core.Interfaces.Offers;

namespace FunPay.Client.Core.Models.Offers
{
  public class Offer : IOffer
  {
    public long Id { get; set; }
    public string Title { get; set; }
    public decimal Price { get; set; }
    public string Description { get; set; }
    public int NodeId { get; set; }
    public int ServerId { get; set; }
    public bool IsActive { get; set; }
    public Location Location { get; set; }
  }
}