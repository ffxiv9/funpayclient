﻿using System.Collections.Generic;
using FunPay.Client.Core.Interfaces.Offers;

namespace FunPay.Client.Core.Models.Offers
{
  public class OffersResponse : BaseFunPayResponse
  {
    public IEnumerable<IOffer> Offers { get; set; }
  }
}