﻿namespace FunPay.Client.Core.Models
{
  public class BaseFunPayResponse
  {
    public int ErrorCode { get; set; }
    public string ErrorMessage { get; set; }

    public bool HasErrors()
    {
      return ErrorCode != 0;
    }
  }
}