﻿namespace FunPay.Client.Core.Models.Data
{
  public class AppDataResponse : BaseFunPayResponse
  {
    public ApplicationData JsonAppData { get; set; }
  }
}