﻿using Newtonsoft.Json;

namespace FunPay.Client.Core.Models.Data
{
  public class ApplicationData
  {
    [JsonProperty("csrf-token")] public string CsrfToken { get; set; }
    [JsonProperty("locale")] public string Locale { get; set; }
    [JsonProperty("userId")] public long UserId { get; set; }
    [JsonProperty("webpush")] public Webpush Webpush { get; set; }
  }

  public class Webpush
  {
    [JsonProperty("app")] public string App { get; set; }
    [JsonProperty("enabled")] public bool Enabled { get; set; }
    [JsonProperty("hwid-required")] public bool HwidRequired { get; set; }
  }
}