﻿using Newtonsoft.Json;

namespace FunPay.Client.Core.Models.Chat
{
  public class ChatRequest
  {
    [JsonProperty("action")] public string Action { get; set; }
    [JsonProperty("data")] public Data Data { get; set; }
  }

  public class Data
  {
    [JsonProperty("node")] public string Node { get; set; }

    [JsonProperty("last_message", DefaultValueHandling = DefaultValueHandling.Ignore)]
    public long LastMessage { get; set; }

    [JsonProperty("content")] public string Content { get; set; }
  }
}