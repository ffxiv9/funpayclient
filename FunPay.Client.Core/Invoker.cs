﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FunPay.Client.Core.Handlers;
using FunPay.Client.Core.Models.Sales;
using Serilog;

namespace FunPay.Client.Core
{
  public class Invoker
  {
    private readonly FunPayClient client;
    private readonly Dictionary<string, Status> sales = new Dictionary<string, Status>();

    public Invoker(string goldenKey)
    {
      if (string.IsNullOrWhiteSpace(goldenKey))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(goldenKey));
      client = new FunPayClient(goldenKey);
    }

    public async Task MonitoringSales()
    {
      var handler1 = new PaidSaleHandler(client);
      var handler2 = new ClosedSaleHandler(client);
      handler1.SetNextHandler(handler2);
      foreach (var newSale in (await client.GetSales()).Where(s => IsStatusChanged(s.Id, s.Status)))
        SetHandled(newSale.Id, newSale.Status);

      Log.Information("Monitoring sales activated.");
      await Task.Delay(TimeSpan.FromMinutes(1));
      var random = new Random();
      while (true)
      {
        try
        {
          var newSales = (await client.GetSales()).Where(s => IsStatusChanged(s.Id, s.Status)).ToList();
          foreach (var newSale in newSales)
          {
            await handler1.Handle(newSale);
            SetHandled(newSale.Id, newSale.Status);
          }
        }
        catch (Exception ex)
        {
          Log.Error(ex.Message + Environment.NewLine + ex.StackTrace);
        }

        await Task.Delay(TimeSpan.FromSeconds(random.Next(30, 120)));
      }
    }

    private bool IsStatusChanged(string orderId, Status status)
    {
      if (sales.ContainsKey(orderId)) return sales[orderId] != status;

      return true;
    }

    private void SetHandled(string orderId, Status status)
    {
      if (sales.ContainsKey(orderId))
        sales[orderId] = status;
      else
        sales.Add(orderId, status);
    }
  }
}