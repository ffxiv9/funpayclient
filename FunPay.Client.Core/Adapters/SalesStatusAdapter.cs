﻿using System;
using FunPay.Client.Core.Models.Sales;

namespace FunPay.Client.Core.Adapters
{
  public class SalesStatusAdapter
  {
    public static Status Adapt(string text)
    {
      if (string.IsNullOrWhiteSpace(text))
        return Status.Unknown;
      if (Enum.IsDefined(typeof(Status), text))
        return Enum.Parse<Status>(text);
      var status = text.ToLower();
      if (status.Equals("оплачен"))
        return Status.Paid;
      if (status.Equals("закрыт"))
        return Status.Closed;
      return status.Equals("возврат") ? Status.Paid : Status.Unknown;
    }
  }
}