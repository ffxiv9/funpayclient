using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using FunPay.Client.Core.Adapters;
using FunPay.Client.Core.Enums;
using FunPay.Client.Core.Interfaces.Offers;
using FunPay.Client.Core.Models;
using FunPay.Client.Core.Models.Chat;
using FunPay.Client.Core.Models.Data;
using FunPay.Client.Core.Models.Offers;
using FunPay.Client.Core.Models.Sales;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Serilog;

namespace FunPay.Client.Core
{
  public class FunPayClient
  {
    private readonly CookieContainer _cookies;
    private readonly HttpClient _httpClient;

    public FunPayClient(string session)
    {
      _cookies = new CookieContainer();
      _cookies.Add(new Cookie("golden_key", session, "/", ".funpay.ru"));
      _cookies.Add(new Cookie("locale", "en", "/", ".funpay.ru"));

      _httpClient = new HttpClient(new HttpClientHandler {UseCookies = true, CookieContainer = _cookies}, true)
        {BaseAddress = new Uri("https://funpay.ru/")};
      _httpClient.DefaultRequestHeaders.UserAgent.ParseAdd(
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36");
    }

    private long UserId { get; set; }

    public async Task<bool> Delete(IOffer offerId)
    {
      return await SaveInternal(offerId, true);
    }

    public async Task<IEnumerable<IOffer>> GetActiveOffers()
    {
      var dataSelector = new Func<string, OffersResponse>(s =>
      {
        var document = new HtmlDocument();
        document.LoadHtml(s);
        return new OffersResponse
        {
          Offers = document.DocumentNode.SelectNodes("//a[@class='tc-item']")?.Select(node => new Offer
                     {
                       Id = Convert.ToInt64(HttpUtility.ParseQueryString(new Uri(node.Attributes["href"].Value).Query)
                         .Get("id")),
                       Title = node.SelectSingleNode(".//div[@class='tc-desc-text']").InnerText
                     }).Where(offer => offer.Id != 0 && !string.IsNullOrWhiteSpace(offer.Title))
                     .ToList() ??
                   Enumerable.Empty<Offer>()
        };
      });
      var response = await GetFunPayDataAsync(HttpMethod.Get, UrlGetUserProfile, dataSelector);
      return response?.Offers;
    }

    public async Task<IOffer> GetOffer(long offerId)
    {
      if (offerId == 0)
        throw new ArgumentException(nameof(offerId));

      var dataSelector = new Func<string, OfferResponse>(s =>
      {
        var html = (string) JsonConvert.DeserializeObject<dynamic>(s).html;
        var document = new HtmlDocument();
        document.LoadHtml(html);
        var rootNode = document.DocumentNode;
        return new OfferResponse
        {
          Offer = new Offer
          {
            Id = offerId,
            Title = rootNode.SelectSingleNode("//input[@name='fields[summary]']").Attributes["value"].Value,
            Price = decimal.Parse(rootNode.SelectSingleNode("//input[@name='price']").Attributes["value"].Value,
              CultureInfo.InvariantCulture),
            IsActive = rootNode.SelectSingleNode("//input[@name='active']").Attributes["checked"] != null,
            Description = rootNode.SelectSingleNode("//textarea[@name='fields[desc]']").InnerText,
            NodeId = int.Parse(rootNode.SelectSingleNode("//input[@name='node_id']").Attributes["value"].Value),
            Location = Location.Trade,
            ServerId = int.Parse(rootNode.SelectSingleNode("//select[@name='server_id']/option[@selected]")
              .Attributes["value"].Value)
          }
        };
      });

      return (await GetFunPayDataAsync(HttpMethod.Get, UrlOfferEdit, dataSelector, new[]
      {
        new KeyValuePair<string, string>("offer", offerId.ToString())
      }, true)).Offer;
    }

    public async Task<IEnumerable<Sale>> GetSales()
    {
      var dataSelector = new Func<string, SalesResponse>(s =>
      {
        var document = new HtmlDocument();
        document.LoadHtml(s);
        return new SalesResponse
        {
          Sales = document.DocumentNode
            .SelectNodes("//div[@class='dyn-table-body']/a[contains(@class,'tc-item')]")?.Select(
              n =>
              {
                var status = n.SelectSingleNode("./div[contains(@class,'tc-status')]")
                  .InnerText.Trim();
                return new Sale
                {
                  Id = n.SelectSingleNode("./div[@class='tc-order']").InnerText.Trim(),
                  Title = n.SelectSingleNode("./div[@class='order-desc']/div").InnerText.Trim(),
                  BuyerId = long.Parse(new Regex(@"users.(\d+)")
                    .Match(n.SelectSingleNode("./div[@class='tc-user']//div[@data-href]")
                      .Attributes["data-href"]
                      .Value).Groups[1].Value),
                  Status = SalesStatusAdapter.Adapt(status)
                };
              }).ToList() ?? Enumerable.Empty<Sale>()
        };
      });
      var response = await GetFunPayDataAsync(HttpMethod.Get, UrlOrdersTrade, dataSelector);
      return response?.Sales;
    }

    public async Task<bool> Save(IOffer offer)
    {
      return await SaveInternal(offer);
    }

    public async Task SendMessage(long targetUserId, string text)
    {
      var appData = await GetApplicationData();
      if (UserId == 0) UserId = appData.UserId;

      var requestData = new[]
      {
        new KeyValuePair<string, string>("request", JsonConvert.SerializeObject(new ChatRequest
        {
          Action = "chat_message",
          Data = new Data
            {Content = text, Node = $"users-{Math.Min(targetUserId, UserId)}-{Math.Max(targetUserId, UserId)}"}
        })),
        new KeyValuePair<string, string>("csrf_token", appData.CsrfToken)
      };
      var dataSelector = new Func<string, BaseFunPayResponse>(s =>
      {
        var errorMessage = (string) JsonConvert.DeserializeObject<dynamic>(s)?.response?.error;
        if (string.IsNullOrWhiteSpace(s))
          errorMessage = "The service returned an empty answer.";

        return string.IsNullOrWhiteSpace(errorMessage)
          ? new BaseFunPayResponse()
          : new BaseFunPayResponse {ErrorCode = 1, ErrorMessage = errorMessage};
      });
      await GetFunPayDataAsync(HttpMethod.Post, UrlRunner, dataSelector, requestData, true);
    }

    private async Task<ApplicationData> GetApplicationData()
    {
      var dataSelector = new Func<string, AppDataResponse>(s =>
      {
        var document = new HtmlDocument();
        document.LoadHtml(s);
        var applicationData = JsonConvert.DeserializeObject<ApplicationData>(HttpUtility.HtmlDecode(document
          .DocumentNode
          .SelectSingleNode("//body[@data-app-data]").Attributes["data-app-data"].Value));

        return string.IsNullOrWhiteSpace(applicationData?.CsrfToken)
          ? new AppDataResponse {ErrorMessage = "Application data has not been downloaded.", ErrorCode = 1}
          : new AppDataResponse {JsonAppData = applicationData};
      });
      var response = await GetFunPayDataAsync(HttpMethod.Get, UrlIndex, dataSelector);
      return response?.JsonAppData;
    }

    private async Task<TResponse> GetFunPayDataAsync<TResponse>(HttpMethod method,
      string requestUrl, Func<string, TResponse> dataSelector,
      IEnumerable<KeyValuePair<string, string>> requestObj = null, bool ajax = false)
      where TResponse : BaseFunPayResponse
    {
      var requestMessage = new HttpRequestMessage(method, requestUrl);
      if (method == HttpMethod.Get)
      {
        if (requestObj != null)
        {
          requestUrl += "?" + string.Join("&",
            requestObj.Select(pair =>
              $"{HttpUtility.UrlEncode(pair.Key)}={HttpUtility.UrlEncode(pair.Value)}").ToArray());
          requestMessage.RequestUri = new Uri(requestUrl, UriKind.Relative);
        }
      }
      else if (method == HttpMethod.Post)
      {
        if (requestObj != null)
          requestMessage.Content = new FormUrlEncodedContent(requestObj);
      }
      else
      {
        throw new NotImplementedException();
      }

      if (ajax)
        requestMessage.Headers.Add("X-Requested-With", "XMLHttpRequest");

      var responseMessage = await _httpClient.SendAsync(requestMessage);

      if (!responseMessage.IsSuccessStatusCode)
        throw new FunPayException(
          $"FunPay was unable to process the request. HTTP Status code: {responseMessage.StatusCode}");

      var responseData = await responseMessage.Content.ReadAsStringAsync(); // Get data
      try
      {
        var responseObject = dataSelector(responseData); // Get object
        if (responseObject.HasErrors())
          throw new FunPayException(
            $"The request was not processed. FunPay's system indicated the error. Request code: {responseObject.ErrorCode}, message: {responseObject.ErrorMessage}");

        return responseObject;
      }
      catch (FunPayException)
      {
        throw;
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message + Environment.NewLine + ex.StackTrace);
        throw new FunPayException("I can't bring the object to the FunPay response. Support for this developer");
      }
    }

    private async Task<bool> SaveInternal(IOffer offer, bool delete = false)
    {
      if (offer == null)
        throw new ArgumentException(nameof(offer));
      var appData = await GetApplicationData();
      if (string.IsNullOrWhiteSpace(appData?.CsrfToken))
        throw new InvalidOperationException("Csrf Token is empty.");

      var requestData = new List<KeyValuePair<string, string>>
      {
        new KeyValuePair<string, string>("offer_id", offer.Id.ToString()),
        new KeyValuePair<string, string>("node_id", offer.NodeId.ToString()),
        new KeyValuePair<string, string>("server_id", offer.ServerId.ToString()),
        new KeyValuePair<string, string>("fields[summary]", offer.Title),
        new KeyValuePair<string, string>("fields[desc]", offer.Description),
        new KeyValuePair<string, string>("price", offer.Price.ToString(CultureInfo.InvariantCulture)),
        new KeyValuePair<string, string>("location", offer.Location.ToString().ToLower()),
        new KeyValuePair<string, string>("csrf_token", appData.CsrfToken)
      };
      if (offer.IsActive)
        requestData.Add(new KeyValuePair<string, string>("active", "on"));
      if (delete)
        requestData.Add(new KeyValuePair<string, string>("deleted", "1"));

      var dataSelector = new Func<string, BaseFunPayResponse>(s =>
      {
        var content = JsonConvert.DeserializeObject<dynamic>(s);
        var hasErrors = !(bool) content.done;
        return new BaseFunPayResponse
        {
          ErrorCode = hasErrors ? 1 : 0,
          ErrorMessage = hasErrors
            ? (string) content.error + Environment.NewLine + string.Join(Environment.NewLine, content.errors.ToString())
            : string.Empty
        };
      });
      var response = await GetFunPayDataAsync(HttpMethod.Post, UrlOfferSave, dataSelector, requestData, true);
      return !response.HasErrors();
    }

    #region Urls

    private const string UrlGetUserProfile = "en/users/357813/";
    private const string UrlOfferSave = "en/lots/offerSave";
    private const string UrlOfferEdit = "en/lots/offerEdit";
    private const string UrlRunner = "en/runner/";
    private const string UrlIndex = "en/";
    private const string UrlOrdersTrade = "en/orders/trade";

    #endregion
  }


  internal class FunPayException : InvalidOperationException
  {
    public FunPayException(string message) : base(message)
    {
    }
  }
}