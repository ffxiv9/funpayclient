﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;

namespace FunPay.Client.Core
{
  internal static class Application
  {
    private static async Task Main(string[] args)
    {
      ConfigureLogger();
      var configText = await File.ReadAllTextAsync("appsettings.json");
      var configuration = JsonConvert.DeserializeObject<Configuration>(configText);
      ValidateConfiguration(configuration);

      var invoker = new Invoker(configuration.GoldenKey);
      await invoker.MonitoringSales();
    }

    private static void ValidateConfiguration(Configuration configuration)
    {
      if (string.IsNullOrWhiteSpace(configuration?.GoldenKey))
        throw new Exception("Configuration is invalid");
    }

    private static void ConfigureLogger()
    {
      const string fullTemplate =
        "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} ({Bot}{SourceContext}) [{Level:u3}] {Message:lj}{NewLine}{Exception}";
      const string shortTemplate =
        "{Timestamp:yyyy-MM-dd HH:mm:ss} ({Bot}{SourceContext}) {Level:u3} {Message}{NewLine}{Exception}";

      Log.Logger = new LoggerConfiguration().Enrich.FromLogContext()
        .WriteTo.Logger(l => l.WriteTo.ColoredConsole(outputTemplate: shortTemplate)
          .WriteTo.File("app.log", outputTemplate: fullTemplate)
          .WriteTo.File("errors.log", LogEventLevel.Error, fullTemplate)
        )
        .CreateLogger();
    }
  }
}