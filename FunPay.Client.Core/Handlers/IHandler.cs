﻿using System.Threading.Tasks;
using FunPay.Client.Core.Models.Sales;

namespace FunPay.Client.Core.Handlers
{
  public interface ISaleHandler
  {
    void SetNextHandler(ISaleHandler handler);
    Task Handle(Sale sale);
  }
}