﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FunPay.Client.Core.Models.Sales;
using Serilog;

namespace FunPay.Client.Core.Handlers
{
  public class PaidSaleHandler : BaseSaleHandler
  {
    public PaidSaleHandler(FunPayClient client) : base(client)
    {
    }

    public override async Task Handle(Sale sale)
    {
      if (sale.Status == Status.Paid)
      {
        if (string.IsNullOrWhiteSpace(sale.Title))
        {
          Log.Error($"Title is empty. Sale {sale.Id} {sale.BuyerId} {sale.Status}");
          return;
        }

        Log.Information(
          $"New sale. id: {sale.Id} buyerId: {sale.BuyerId} title: {sale.Title} status: {sale.Status}\n");

        var accountCount = 1;
        try
        {
          var count = new Regex(@"(\d+)\sАккаунт").Match(sale.Title).Groups[1].Value;
          if (!string.IsNullOrWhiteSpace(count))
            accountCount = int.Parse(count);
        }
        catch (Exception)
        {
          // ignored
        }

        var accountsPath = File.ReadAllLines("games.txt")
          .FirstOrDefault(s => sale.Title.Contains(s.Split(";")[0]));
        if (!string.IsNullOrWhiteSpace(accountsPath))
        {
          accountsPath = accountsPath.Split(";")[1];
          var accountsFiles = Directory.GetFiles(accountsPath, "*.txt", SearchOption.AllDirectories);
          if (accountsFiles.Length < accountCount)
            throw new InvalidOperationException("Not enough files.");

          for (var i = 0; i < accountCount; i++)
          {
            var file = accountsFiles[i];
            var info = File.ReadAllText(file).Split(":");

            var success = false;
            do
            {
              Log.Information($"Sending message to {sale.BuyerId}");
              try
              {
                await Client.SendMessage(sale.BuyerId,
                  $"#{i + 1}: Email: {info[0]}\nEpicGames password: {info[1]}\nEmail password: {info[3]}");
                success = true;
              }
              catch (Exception e)
              {
                Log.Error(e.Message + Environment.NewLine + e.StackTrace);
                await Task.Delay(TimeSpan.FromSeconds(10));
              }
            } while (!success);

            var soldDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Sold");
            Directory.CreateDirectory(soldDirectory);
            Directory.Move(Path.GetDirectoryName(file),
              Path.Combine(soldDirectory, Directory.GetParent(file).Name));
          }
        }

        await SetAllMonitoringOrdersActive();
      }
      else
      {
        await NextHandler.Handle(sale);
      }
    }

    private async Task SetAllMonitoringOrdersActive()
    {
      const string offersTxt = "offers.txt";
      var monitoringOffers = File.ReadAllLines(offersTxt).Select(long.Parse).ToArray();
      var success = false;
      do
      {
        try
        {
          var currentlyActive = await Client.GetActiveOffers();
          var isNotActive = monitoringOffers.Except(currentlyActive.Select(offer => offer.Id));
          foreach (var monitoringOffer in isNotActive)
          {
            var offer = await Client.GetOffer(monitoringOffer);
            Log.Information($"Set active to {offer.Title}.");
            offer.IsActive = true;
            await Client.Save(offer);
          }

          success = true;
        }
        catch (Exception ex)
        {
          Log.Error(ex.Message + Environment.NewLine + ex.StackTrace);
          await Task.Delay(TimeSpan.FromSeconds(10));
        }
      } while (!success);
    }
  }
}