﻿using System.Threading.Tasks;
using FunPay.Client.Core.Models.Sales;

namespace FunPay.Client.Core.Handlers
{
  public abstract class BaseSaleHandler : ISaleHandler
  {
    protected readonly FunPayClient Client;
    protected ISaleHandler NextHandler;

    protected BaseSaleHandler(FunPayClient client)
    {
      Client = client;
    }

    public void SetNextHandler(ISaleHandler handler)
    {
      NextHandler = handler;
    }

    public abstract Task Handle(Sale sale);
  }
}