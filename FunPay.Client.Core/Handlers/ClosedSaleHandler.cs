﻿using System.Threading.Tasks;
using FunPay.Client.Core.Models.Sales;

namespace FunPay.Client.Core.Handlers
{
  public class ClosedSaleHandler : BaseSaleHandler
  {
    public ClosedSaleHandler(FunPayClient client) : base(client)
    {
    }

    public override async Task Handle(Sale sale)
    {
      if (sale.Status == Status.Closed)
        await Client.SendMessage(sale.BuyerId, "Спасибо за покупку!");
      else if (NextHandler != null)
        await NextHandler.Handle(sale);
    }
  }
}