# FunPayClient

FunPay Client is a console application that helps you track sales and send items to customers through funpay.

### Installation
Requires [.net core v3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)  to run.
Create an `appsettings.json` file with your golden key, which can be found in the cookie headers on funpay.

### Development

Want to contribute? Great!

#### Building for source
For production release:
```sh
$ dotnet publish -c Release 
```

License
----

MIT